+++
git = "https://gitlab.dune-project.org/staging/dune-typetree"
group = ["extension"]
maintainers = "Steffen Müthing"
module = "dune-typetree"
requires = ["dune-common"]
title = "dune-typetree"
+++

Dune-Typetree
=============

TypeTree is a template library for constructing and operating on statically
typed trees of objects. 
