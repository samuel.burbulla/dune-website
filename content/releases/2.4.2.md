+++
major_version = 2
minor_version = 4
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions"]
patch_version = 2
title = "Dune 2.4.2"
version = "2.4.2"
signed = 1
doxygen_branch = ["v2.4.2"]
doxygen_url = ["/doxygen/2.4.2"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.4.2"]
doxygen_modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.4.2 - Release Notes

DUNE 2.4.2 is a bug fix release to keep DUNE 2.4 working for recent systems. It is advised to move to DUNE 2.5.


## Dependencies

In order to build this version of DUNE you need at least the following software:

* CMake (>=2.8.6), preferably (>=2.8.12)
* pkg-config
* a standard compliant C++ compiler, tested are g++ (>=4.4) and Clang (>=3.4);
  recent versions of ICC (>= 15) should work, older versions like 14.0.3 needs patching of system headers and is discouraged

## Build System

### Bug Fixes

*   Fix location where CMake installs aclocal.
*   Fix handling of Doxygen when using CMake.

## dune-common

## dune-geometry

## dune-grid

*   Add support for UG 3.13.

### Bug Fixes

*   Fix vertex reordering to boundary segment vertices when using UG.

## dune-istl

*   Ignore Boost 1.61 and newer because they are not compatible. Only `MultiTypeBlockVector` is affected. If it is required, use an older version of Boost or switch to Dune 2.5 which impements `MultiTypeBlockVector` without Boost.
*   Backport support for SuperLU 5.
*   `io.hh` contains a vector file writer, compatible with Octave and Matlab.

## dune-localfunctions
