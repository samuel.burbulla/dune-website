+++
date = "2020-01-19T22:40:00+01:00"
version = "2.7.0"
major_version = "2"
minor_version = "7"
patch_version = "0"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto", "dune-localfunctions"]
signed = 1
title = "Dune 2.7.0"
doxygen_branch = ["v2.7.0"]
doxygen_url = ["/doxygen/2.7.0"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.7.0"]
support = ["linux","windows"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.7 - Release Notes

## Module dune-common:

- Added fallback implementation to C++20 feature: `std::identity`.

- A helper class `TransformedRangeView` was added representing a
  transformed version of a given range using an unary transformation
  function. The transformation is done on the fly leaving the wrapped
  range unchanged.

- `dune-common` now provides an implementation of `std::variant` for all compilers
  that support C++14.  It is contained in the file `dune/common/std/variant.hh`,
  in the namespace `Dune::Std::`.  If your compiler does support C++17 the
  implementation in `dune-common` is automatically disabled, and the official
  implementation from the standard library is used instead.

- By popular demand, dense vectors and matrices like `FieldVector` and `FieldMatrix`
  now have additional operators.  In particular, there are
  - Vector = - Vector
  - Matrix = - Matrix
  While these two work for any vector or matrix class that inherits from `DenseVector`
  or `DenseMatrix`, the following additional methods only work for `FieldVector`:
  - Vector = Scalar * Vector
  - Vector = Vector * Scalar
  - Vector = Vector / Scalar
  Correspondingly, the `FieldMatrix` class now has
  - Matrix = Matrix + Matrix
  - Matrix = Matrix - Matrix
  - Matrix = Scalar * Matrix
  - Matrix = Matrix * Scalar
  - Matrix = Matrix / Scalar
  - Matrix = Matrix * Matrix
  Note that the operators
  - Vector = Vector + Vector
  - Vector = Vector - Vector
  have been introduced earlier.

- There is now (finally!) a method `power` in the file `math.hh` that computes
  powers with an integer exponent, and is usable in compile-time expressions.
  The use of the old power methods in `power.hh` is henceforth discouraged.

-   `FieldMatrix` and `FieldVector` are now [trivially copyable types]
    if the underlying field type is trivially copyable.

    As a consequence the copy assignment operator of the `DenseVector`
    class can no longer be used; just avoid going through
    `DenseVector` and use the real vector type instead
    (e.g. `FieldVector`).

    [trivially copyable types]: https://en.cppreference.com/w/cpp/named_req/TriviallyCopyable

### Deprecations and removals

- The `VectorSize` helper has been deprecated.  The `size()` method of
  vectors should be called directly instead.

- Drop support for Python 2. Only Python 3 works with Dune 2.7.

- Support for older version than METIS 5.x and ParMETIS 4.x is deprecated and will be
  removed after Dune 2.7.

- Deprecated header `dune/common/parallel/collectivecommunication.hh` which will be
  removed after Dune 2.7. Use dune/common/parallel/communication.hh instead!

- Deprecated header `dune/common/parallel/mpicollectivecommunication.hh` which will be
  removed after Dune 2.7. Use dune/common/parallel/mpicommunication.hh instead!

### build-system

- When run with an absolute build directory, `dunecontrol` now exposes the root build
  directory to CMake in the variable `DUNE_BUILD_DIRECTORY_ROOT_PATH`.

  See core/dune-common!542

- The `dune_symlink_to_sources_files` CMake function now has a `DESTINATION` argument.

- Dune no longer applies architecture flags detected by the Vc library
  automatically.  This applies to all targets that link to Vc explicitly (with
  `add_dune_vc_flags()`) or implicitly (with `dune_enable_all_packages()`).
  If you do want to make use of extended architecture features, set the
  architecture explicitly in the compiler options, e.g. by specifying
  ```sh
  CMAKE_FLAGS="-DCMAKE_CXX_FLAGS=-march=native"
  ```
  in your opts-file.  Vc also sets compiler options to select a particular C++
  abi (`-fabi-version` and `-fabi-compat-version`), these continue to be
  applied automatically.

  See core/dune-common!677

- `FindParMETIS.cmake` assumes METIS was found first using `FindMETIS.cmake` and does not
  longer try to find METIS itself.

- The `inkscape_generate_png_from_svg` CMake function is deprecated and will be removed
  after 2.7.

- LaTeX documents can now be built using `latexmk` with the help of UseLatexmk.cmake's
  `add_latex_document`. `dune_add_latex_document` will use the new way of calling
  LaTeX when the first argument is `SOURCE`. As a side effect, in-source builds are
  supported, too. The old function call and UseLATEX.cmake are deprecated and will be
  removed after 2.7.

  See core/dune-common!594

- The build system has learned some new tricks when creating or looking for the Python virtualenv:
  When using an absolute build directory with `dunecontrol`, the virtualenv will now be placed
  directly inside the root of the build directory hierarchy in the directory `dune-python-env`.
  This should make it much easier to actually find the virtualenv and also avoids some corner
  cases where the build system would create multiple virtualenvs that did not know about each
  other. This behavior can be disabled by setting
  `DUNE_PYTHON_EXTERNAL_VIRTUALENV_FOR_ABSOLUTE_BUILDDIR=0`.
  If you need even more precise control about the location of the virtualenv, you can now also
  directly set the CMake variable `DUNE_PYTHON_VIRTUALENV_PATH` to the directory in which to
  create the virtualenv.

## Module dune-geometry:

- The reference elements have a new method `subEntities`. The result of
  `referenceELement.subEntities(i,codim, c)` is an iterable range
  containing the indices of all codim-`c` subentities of the subentity
  `(i,codim)`, e.g., the vertices of an edge. The range also provides
  the methods `size()` and `contains()`.
- The methods `GeometryType(int)` and `GeometryType(unsigned int)` have been deprecated
  and will be removed after the release of dune-geometry 2.7.  Instead, please now use
  `GeometryTypes::cube(dim)` to construct one- or two-dimensional `GeometryType` objects.
- Geometry implementations now export a type `Volume` that is used for the return
  value of the `volume` methods.  So does the generic `ReferenceElement` implementation.
-   More efficient quadrature rules for simplices are available that
    need less quadrature points to achive the same order.  For now these
    have to be explicitly requested:
    ```c++
    auto&& rule = Dune::QuadratureRules<...>::rule(..., Dune::QuadratureType::GaussJacobi_n_0);
    ```
    See [!127].

    [!127]: https://gitlab.dune-project.org/core/dune-geometry/merge_requests/127

## Module dune-grid:

- The `YaspGrid` class has a new constructor that takes a `Coordinates`
  object as its first argument.  This object can be of type `EquidistantCoordinates`,
  `EquidistantOffsetCoordinates`, or `TensorProductCoordinates`,
  and encapsulates the domain and element sizes.  Previously, this data
  was given directly to different constructors of `YaspGrid`,
  and the constructor had to match the `Coordinates` object used
  as the second template argument of the `YaspGrid` class.
  The new constructor subsumes the previous three, and makes sure
  by design that the correct information is passed at `YaspGrid`
  construction.

- The `GridFactory` exports the type `Communication` and corresponding object
  `comm()` that is used to create the grid. In the `GridFactoryInterface`, this
  defaults to the Communication induced by the process-local communicator
  `MPIHelper::getLocalCommunicator()`, while `UGGrid`'s factory returns the
  Communication of the grid. This can be used to steer the grid creation
  process, see `dune/grid/io/file/gmshreader.hh` for an example.

- The number type used by a `BoundarySegment` is not hard-wired to `double`
  anymore.

- The `Grid::getRealImplementation` member function has been deprecated.
  Use the `impl()` member function of the facade classes directly instead.

- The `AlbertaGrid::getRealIntersection` member function has been deprecated.
  Use the `impl()` member function of the intersection class directly instead.

- The return type of all file reader methods that return a grid has been changed
  from a plain C pointer to the custom pointer class `ToUniquePtr<Grid>`
  (from the `dune-common` module).  Values of this pointer class cast to
  C pointers (but with a deprecation warning), to `std::unique_ptr`, and to
  `std::shared_ptr`.  This marks the beginning of a transition period.
  In the long run, the methods are planned to return objects of type
  `std::unique_ptr`, to make it obvious that the calling code receives
  the ownership of the grid object.  For the time being the calling code
  can still store the return value in a C pointer, but that possibility
  will go away.

- Likewise, the return type of the `GridFactory::createGrid`method has been changed
  from a plain C pointer to the custom pointer class `ToUniquePtr<Grid>`.
  In the long run, the method is planned to return objects of type
  `std::unique_ptr`, to make it obvious that the calling code receives
  the ownership of the grid object.  For the time being the calling code
  can still store the return value in a C pointer, but that possibility
  will go away.  While this procedure allows full backward compatibility
  for code that calls `GridFactory::createGrid`, implementors or third-party
  grid implementations will need to update their implementations of
  `GridFactory::createGrid`.

- The `VTKWriter`s now support custom output data precision for functions
  (via the provided `FieldInfo`), `VTKFunction`s  (via a new virtual interface method `precision()`),
  and coordinates (via an argument to the writer's constructor) that can be
  chosen at runtime. Any field can now choose between VTK's `Float32`, `Float64`,
  `Int32`, `UInt8`, and `UInt32`, represented as `Dune::VTK::Precision::float32`/`float64`/`int32`/`uint8`/`uint32`,
  the `DataArrayWriter` selects the correct type at runtime. The default for
  functions and coordinate is `Float32` as before.

- The `VTKWriter` now supports writing functions that can only be evaluated globally,
  i.e., functions that are not defined with respect to the grid.  Such functions
  will be sampled on the grid vertices (`addVertexData`) or grid element centers (`addCellData`).

- The Capability `hasBackupRestoreFacilities<GeometryGrid<HG, CoordFunction>>`
  now returns `false` in case the `CoordFunction` is not default-constructible.

- The `Geometry` interface now provides the type `Volume` for the return value of the
  method of the same name.  Note that this may be different from `ctype` if you care
  about dimensions.  In that case `ctype` is a length, and not appropriate for
  a quantity that is a volume.

- The `VTKWriter` writer now truncates subnormal floating point values to 0 when writing ASCII files
  (`DUNE::VTK::ascii`). This avoids Paraview crashes on macOS. For this reasons, most VTK files written
  by DUNE 2.7 will differ from the same file written in DUNE 2.6. If you are using VTK files for testing
  results, make sure to use fuzzy float comparisons!

- The `VTKSequenceWriter` now exposes the function `clear()` of the associated `VTKWriter`

- The `VTKSequenceWriter` allows to get and set the time steps storage, which enables serialization
  of a sequence writer. The feature can be used to restart a sequence.

- UG 3.x is no longer supported. Use dune-uggrid instead.

## Module dune-grid-howto:


## Module dune-istl:

- New `SolverFactory` for generating sequential direct or iterative solvers and
  preconditioners from a `ParameterTree` configuration.

- `BDMatrix` objects now have the method `solve`, which implements that
  canonical way to solve block-diagonal linear systems.

- The class `VariableBlockVector::CreateIterator` is a true STL output iterator now.
  This means that you can use STL algorithms like `std::fill` or `std::copy`
  to set the block sizes.

- `MultiTypeBlockVector<Args...>` now inherits the constructors from its
  parent type (`std::tuple<Args...>`). This means you can now also construct
  `MultiTypeBlockVector`s from values or references of BlockVectors.

- All matrix and vector classes can now be instantiated with number types
  directly (A number type is any type for which `Dune::IsNumber<T>::value`
  is true).  For example, you can now use `BlockVector<double>` instead of
  the more cumbersome `BlockVector<FieldVector<double,1> >`.  Similarly, you can use
  `BCRSMatrix<double>` instead of `BCRSMatrix<FieldMatrix<double,1,1>>`.
  The old forms still work, and `FieldVector` and `FieldMatrix` types with
  a single entry can still be cast to their `field_type`.  Therefore, the
  change is completely backward-compatible.

- Added a right-preconditioned flexible restarted GMRes solver

- The UMFPack binding use the long int functions to compute larger systems.
  With the \*_dl_\* versions instead of the \*_di_\* versions UMFPACK will not
  have a memory limit of just 2 GiB.

- Support for SuiteSparse's CHOLMOD providing a sparse Cholesky
  factorization.

- The interface methods `dot()` and `norm()` of ScalarProduct are now `const`. You will
  have to adjust the method signatures in your own scalar product implementations.

- `MultiTypeBlockVector` now implements the interface method `N()`, which
  returns the number of vector entries.

- `MultiTypeBlockVector` now implements the interface method `dim()`, which
  returns the number of scalar vector entries.

- `MultiTypeBlockVector::count()` is now `const`

- `SeqILU` can now be used with SIMD data types.

### Deprecations and removals

- Deprecated support for SuperLU 4.x. It will be removed after Dune 2.7.

- Deprecated the preconditioner implementations `SeqILU0` and `SeqILUn`.
  Use `SeqILU` instead, which implements incomplete LU decomposition
  of any order.

- The method `setSolverCategory` of `OwnerOverlapCopyCommunication` is deprecated and
  will be removed after Dune 2.7. The solver category can only be set in the constructor.

- The method `MultiTypeBlockVector::count()` has been deprecated, because its name
  is inconsistent with the name mandated by the `dune-istl` vector interface.

- The method `MultiTypeBlockMatrix::size()` has been deprecated, because its name
  is inconsistent with the name mandated by the `dune-istl` vector interface.

## Module dune-localfunctions:

* The header `lagrange.hh` now includes all headers of all Lagrange implementations,
  not just the ones with run-time order.

* Introduce a run-time polymorphic container `LocalFiniteElementVariant`.
  Much like `std::variant`, it implements a type-safe
  union of different `LocalFiniteElement` implementations.  Elements of type
  `LocalFiniteElementVariant` can hold one object from a list of types
  given as template parameters.  These types must be implementations of
  the `LocalFiniteElement` interface, and the container will in turn
  implement this interface.

  Such a `variant`-based polymorphism is not as flexible as full type erasure,
  but it is much easier to implement.  What is more, it is believed that in
  many situations the restriction to a fixed set of implementation types
  is not a problem.

* Add support for `operator()` syntax to `interpolate()`. All `interpolate()`
  implementations now support functions `f` that either support `f.evaluate(x,y)`
  or `y = f(x)`.

* Add an implementation of the Crouzeix-Raviart element.

* Add an implementation of the Brezzi-Douglas-Fortin-Marini element.
  The coefficients and interpolation are implemented for arbitrary
  dimension (>1) and order (>0). The actual basis is only implemented
  for dim=2 and order=1,2,3.

  See core/dune-localfunctions!105 and core/dune-localfunctions!145

* Introduce a convenience header `hierarchical.hh` that includes
  all hierarchical FE implementations.

* Introduce a new class `LagrangeSimplexLocalFiniteElement`, which implements
  Lagrange finite elements on simplices with compile-time dimension and order.
  It currently does not cover more general dimension/order situations than
  what is already available in dune-localfunctions, but it gathers the
  plethora of different Pk3DNodal, PkNodal, P1Nodal, etc implementations
  under one single name.

* Introduce new class `BrezziDouglasMariniSimplexLocalFiniteElement`
  (and the same for cubes) that subsumes all currently existing simplex
  BDM element implementations under a single name.  Domain dimension and
  polynomial order are template parameters now.

* Introduce a convenience header `dune/localfunctions/brezzidouglasmarini.hh`
  that includes all BDM implementations.
