+++
date = "2007-03-01"
title = "Dune 1.0beta2 available"
+++

We released the second beta of the three core modules `dune-common`, `dune-grid`, `dune-istl` and the `dune-grid-howto`.

This beta release include a lot of bug fixes. We encourage all users to test and report bugs.

Go to the *download page* to grab the packages.
