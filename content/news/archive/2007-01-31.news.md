+++
date = "2007-01-31"
title = "AmiraMesh available for download."
+++

The AmiraMesh library, which is used to read and write grids in the native format of the Amira visualization software, is now available for [download](http://www.amiravis.com/resources/Ext411-01-libamiramesh/index.html).
