+++
date = "2011-11-08"
title = "New core module *dune-geometry*"
+++

Today we introduced a new core module *dune-geometry*. It is intended for everything related to the computational domain that doesn't really fit into a more specific module like dune-grid or dune-localfunctions. It initially starts out with Dune::GeometryType (from dune-common) and the generic reference elements, the quadrature rules and part of the generic geometries (from dune-grid). Kudos to [Christoph Grüninger](http://www.hydrosys.uni-stuttgart.de/institut/mitarbeiter/person.php?name=1450) for doing much of the work.
